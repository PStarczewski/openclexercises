//0 includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <CL/cl.h>

const int ARRAY_SIZE = 1000;
using namespace std;

//1 Context
cl_context CreateContext() {
	cl_int errNum;
	cl_uint numPlatforms;
	cl_platform_id firstPlatformId;
	cl_context context = NULL;

	//Look for platforms and their ID's,(quantityOfPlatforms, platformsId, summaryNumberOfAvaliablePlatforms)
	errNum = clGetPlatformIDs(1, &firstPlatformId, &numPlatforms);
	if (errNum != CL_SUCCESS || numPlatforms <= 0) {
		cerr << "Failed to find any OpenCL platforms" << endl;
		return NULL;
	}
	//Test info
	cout << firstPlatformId << " i " << numPlatforms << endl;

	//Define context properties, (depends on vendor)an array of(PROP_IDENTIFIER, value, 0)
	cl_context_properties contextProperties[] = {CL_CONTEXT_PLATFORM,
												(cl_context_properties)firstPlatformId,
												0};

	//Context creation(contextproperties, devicetype, callback for errors, data for callback, errorcode)
	context = clCreateContextFromType(contextProperties, CL_DEVICE_TYPE_GPU,
		NULL, NULL, &errNum);
	//Chech whether context was created, if not fallback to CPU.
	if (errNum != CL_SUCCESS) {
		cout << "Could not create GPU context, trying CPU...." << endl;

		context = clCreateContextFromType(contextProperties, CL_DEVICE_TYPE_CPU,
			NULL, NULL, &errNum);
		if (errNum != CL_SUCCESS) {
			cerr << "Failed to create CPU as well." << endl;
			return NULL;
		}
		cout << "CPU!" << endl;
	}
	else {
		cout << "GPU!" << endl;
	}

	return context;
}

//2 Command Queue
cl_command_queue CreateCommandQueue(cl_context context, cl_device_id *device){
	cl_int errNum;
	cl_device_id *devices;
	cl_command_queue commandQueue = NULL;
	size_t deviceBufferSize = -1;

	//Information about context.(context specification, list of devices, size of memory for pointer, pointer to memory, size in bytes)
	errNum = clGetContextInfo(context, CL_CONTEXT_DEVICES, 0, NULL, &deviceBufferSize);
	if (errNum != CL_SUCCESS) {
		cerr << "Failed call to clGetContextInfo" << endl;
		return NULL;
	}
	// Chceck whether there is any device.
	if (deviceBufferSize <= 0) {
		cerr << "No devices available." << endl;
		return NULL;
	}

	//Allocate memory for the device buffer
	devices = new cl_device_id[deviceBufferSize / sizeof(cl_device_id)];

	//Get device id and size of memory pointed.
	errNum = clGetContextInfo(context, CL_CONTEXT_DEVICES, deviceBufferSize, devices, NULL);
	if (errNum != CL_SUCCESS) {
		delete[] devices;
		cerr << "Failed to get device IDs";
		return NULL;
	}

	//Create command queue on device choosen before
	commandQueue = clCreateCommandQueue(context, devices[0], 0, NULL);
	if (commandQueue == NULL) {
		delete[] devices;
		cerr << "Failed to create commandQueue for device 0";
		return NULL;
	}
	//I know i have only 2 devices so i want to know thier id's
	cout << devices[0] << " i " << devices [1] << endl;
	//Point to certain device and delete rest of pointers to free memory.
	*device = devices[0];
	delete[] devices;

	return commandQueue;
}

//3 OpenCL program to generate kernels
cl_program CreateProgram(cl_context context, cl_device_id device, const char* fileName) {
	cl_int errNum;
	cl_program program;

	//Open file with kernel in read-only mode.
	ifstream kernelFile(fileName);
	if (!kernelFile.is_open()) {
		cerr << "Failed to open file for reading" << endl;
		return NULL;
	}

	//Open kernelFile to a buffer
	ostringstream oss;
	oss << kernelFile.rdbuf();
	string srcStdStr = oss.str(); //Get string content
	const char *srcStr = srcStdStr.c_str();//Return pointer to a string

	//Create program with kernel source code.(context, count of arrays with code, pointer to source code, lengths of arrays with code, errcode_ret)
	program = clCreateProgramWithSource(context, 1, (const char**)&srcStr,
		NULL, NULL);
	if (program == NULL) {
		cerr << "Failed to create CL program from source." << endl;
		return NULL;
	}

	//Compile and link kernel program
	errNum = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	//THIS ERROR DOESNT WORK, WHY? 
	if (errNum == CL_SUCCESS) {
		char buildLog[16384];//Size of log data
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
			sizeof(buildLog), buildLog, NULL);

		cerr << "Error in kernel: " << endl;
		cerr << buildLog;
		clReleaseProgram(program);
		return NULL;
	}
	return program;
}

//4 Create memory objects used as arguments to the kernel
// Kernel takes three arguments: result-output, a,b-input
bool CreateMemObjects(cl_context context, cl_mem memObjects[3],
	float* a, float* b) {

	memObjects[0] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(float)* ARRAY_SIZE, a, NULL);
	memObjects[1] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(float)* ARRAY_SIZE, b, NULL);
	memObjects[2] = clCreateBuffer(context, CL_MEM_READ_WRITE,
		sizeof(float)* ARRAY_SIZE, NULL, NULL);

	if (memObjects[0] == NULL || memObjects[1] == NULL || memObjects[2] == NULL) {
		cerr << "Error creating memory objects." << endl;
		return false;
	}
	return true;
}

//5 Memory release
void Cleanup(cl_context context, cl_command_queue commandQueue,
	cl_program program, cl_kernel kernel, cl_mem memObjects[3]) {

	for (int i = 0; i < 3; ++i) {
		if (memObjects[i] != 0)
			clReleaseMemObject(memObjects[i]);
	}
	if (commandQueue != 0)
		clReleaseCommandQueue(commandQueue);
	if (kernel != 0)
		clReleaseKernel(kernel);
	if (program != 0)
		clReleaseProgram(program);
	if (context != 0)
		clReleaseContext(context);
}

//Workflow
int main() 
{
	cl_context context = 0;
	cl_command_queue commandQueue = 0;
	cl_program program = 0;
	cl_device_id device = 0;
	cl_kernel kernel = 0;
	cl_mem memObjects[3] = { 0,0,0 };
	cl_int errNum;
	//1 Create context on platform(may be many)
	context = CreateContext();
	if (context == NULL){
		cerr << "Failed to create OpenCL context." << endl;
		return 1;
	}
	//2 Look for devices and make a commandQueue on them
	commandQueue = CreateCommandQueue(context, &device);
	if (commandQueue == NULL) {
		Cleanup(context, commandQueue, program, kernel, memObjects);
		return 1;
	}
	//3 Create program for kernels
	program = CreateProgram(context, device, "kernel.cl");
	if (program == NULL) {
		Cleanup(context, commandQueue, program, kernel, memObjects);
		return 1;
	}
	//4 Build kernels to execution
	kernel = clCreateKernel(program, "hello_kernel", NULL);
	if (kernel == NULL) {
		cerr << "Failed to create kernel." << endl;
		Cleanup(context, commandQueue, program, kernel, memObjects);
		return 1;
	}
	
	//4,5 Variables
	float result[ARRAY_SIZE];
	float a[ARRAY_SIZE];
	float b[ARRAY_SIZE];

	for (int i = 0; i < ARRAY_SIZE; ++i) {
		a[i] = (float)i;
		b[i] = (float)(i * 2);
	}

	//5 Create memory object for kernel usage
	if (!CreateMemObjects(context, memObjects, a, b)) {
		Cleanup(context, commandQueue, program, kernel, memObjects);
		return 1;
	}

	//Once again some shit i dont get |= is a pipe operator and works as +=
	errNum = clSetKernelArg(kernel, 0, sizeof(cl_mem), &memObjects[0]);
	errNum |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &memObjects[1]);
	errNum |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &memObjects[2]);
	if (errNum != CL_SUCCESS) {
		cerr << "Error setting kernel arguments." << endl;
		Cleanup(context, commandQueue, program, kernel, memObjects);
		return 1;
	}

	//Define sizef of workmemory spaces
	size_t globalWorkSize[1] = { ARRAY_SIZE };
	size_t localWorkSize[1] = { 1 };

	//Enqueue kernels for execution. Execute
	errNum = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL,
		globalWorkSize, localWorkSize, 0, NULL, NULL);
	if (errNum != CL_SUCCESS) {
		cerr << "Error queueing kernel for execution." << endl;
		Cleanup(context, commandQueue, program, kernel, memObjects);
		return 1;
	}

	//Enqueue ReadBuffer. Execute
	errNum = clEnqueueReadBuffer(commandQueue, memObjects[2], CL_TRUE,
		0, ARRAY_SIZE * sizeof(float), result, 0, NULL, NULL);
	if (errNum != CL_SUCCESS) {
		cerr << "Error reading result buffer." << endl;
		Cleanup(context, commandQueue, program, kernel, memObjects);
		return 1;
	}

	//Print results.
	for (int i = 0; i < ARRAY_SIZE; ++i) {
		cout << result[i] << " ";
	}
	cout << endl;
	cout << "Executed succesfully" << endl;

	//Release memory.
	Cleanup(context, commandQueue, program, kernel, memObjects);

	//Just a holder.
	int i;
	cin >> i;
}